package demo.pj.com.answerdemo.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import demo.pj.com.answerdemo.R;
import demo.pj.com.answerdemo.customview.CircleProgress;

public class AnswerFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CircleProgress pb_time;

    public AnswerFragment() {
    }

    public static AnswerFragment newInstance(String param1, String param2) {
        AnswerFragment fragment = new AnswerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            if(null!=tv_title){
                tv_title.setText(mParam1);
            }
        }
    }
    private boolean isShow=false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if(pb_time==null){
                return;
            }
            isShow=true;
            pb_time.setValue(0);
            han.sendEmptyMessageDelayed(1, 100);
        }else{
            isShow=false;
        }
    }

    private TextView tv_title;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_answer, container, false);
        pb_time=view.findViewById(R.id.pb_time);
        tv_title=view.findViewById(R.id.tv_title);
        han.sendEmptyMessageDelayed(1, 100);
        return view;
    }
    Handler han = new Handler() {
        public void handleMessage(android.os.Message msg) {
            pb_time.setValue(msg.what);
            if(isShow){
                han.sendEmptyMessageDelayed(msg.what>=100?0: msg.what+ 1, 100);
            }
        };
    };
}
