package demo.pj.com.answerdemo;

import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import demo.pj.com.answerdemo.adapter.FragmentAdapter;
import demo.pj.com.answerdemo.customview.CircleProgress;

public class MainActivity extends AppCompatActivity {
    private FragmentAdapter fragmentAdapter;
    private ViewPager view_pg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view_pg=findViewById(R.id.view_pg);
        fragmentAdapter=new FragmentAdapter(getSupportFragmentManager());
        view_pg.setAdapter(fragmentAdapter);
    }
}
