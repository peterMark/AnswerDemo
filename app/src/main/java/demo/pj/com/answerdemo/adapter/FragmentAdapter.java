package demo.pj.com.answerdemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import demo.pj.com.answerdemo.fragment.AnswerFragment;

/**
 * Created by pc on 2018/3/28.
 */

public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=AnswerFragment.newInstance(""+position,""+position);
        return fragment;
    }

    @Override
    public int getCount() {
        return 10;
    }
}